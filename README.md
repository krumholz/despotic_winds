# Contents #

This repository contains the scripts necessary to produce the figures for Krumholz et al. (2017). These scripts
make use of [DESPOTIC](https://bitbucket.org/krumholz/despotic), which must be installed before they can be run.

### Contents ###

* `plot_acc_law.py`: produces figures 1-3
* `plot_den_struct.py`: produces figures 4-5
* `plot_momentum.py`: produces figure 6
* `plot_geometry.py`: produces figure 7
* `plot_odepth.py`: produces figure 8
* `plot_CO_line.py`: produces figure 9
* `plot_LTE_int.py`: produces figure 10
* `plot_subcritical.py`: produces figure 11
* `plot_m82.py`: produces figures 12-14 (takes about 10 minutes to run)
* `plot_m82_var.py`: produces figures 15-18 (takes about 10 minutes to run)
* `plot_m82_fcrit.py`: produces figures A1-A2

### License ###

This code is distributed under the terms of the [GNU General Public License](http://www.gnu.org/copyleft/gpl.html)
version 3.0. The text of the license is included in the main directory of the repository as GPL-3.0.txt.

### Contact information ###

If you have any questions, please contact [Mark Krumholz](http://www.mso.anu.edu.au/~krumholz/), 
mark.krumholz@anu.edu.au.