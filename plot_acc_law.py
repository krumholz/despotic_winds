"""
This script plots some example wind acceleration laws.
"""

import numpy as np
import matplotlib.pyplot as plt
from despotic.winds import pwind

# First plot: wind acceleration laws for different geometries and
# fixed Gamma, Mach, x
Gamma = 0.1
M = 30.0
x = -3.0
potential = ['point', 'isothermal']
plab = ['Point', 'Iso']
expansion = ['area', 'intermediate', 'solid angle']
exlab = ['Area', 'Int', 'SA']
a = np.linspace(1, 10, 5000)
plt.figure(1, figsize=(5,3.5))
plt.clf()
linestyle = ['-', '--']
color = ['b', 'g', 'r']
for ls, pot, pl in zip(linestyle, potential, plab):
    for clr, ex, el in zip(color, expansion, exlab):
        w = pwind(Gamma, M, potential=pot, expansion=ex)
        plt.plot(a, w.U(x, a), clr+ls, lw=2, label=pl+', '+el)
plt.subplots_adjust(bottom=0.16, right=0.95)
plt.xlabel(r'$a$')
plt.ylabel(r'$U_a(x)$')
plt.legend(loc='upper left', ncol=2, prop={"size" : 10})
plt.savefig('acc_law_ideal.pdf')

# Second plot: compare wind acceleration laws for the ideal,
# radiation cases
exlab = ['Area', 'Intermediate', 'Solid Angle']
Gamma = 0.1
M = 30.0
tau = [30., 100.0, 300.]
color = ['b', 'g', 'r']
plt.figure(2, figsize=(5,6))
plt.clf()
for i, ex in enumerate(expansion):
    lab1 = []
    lab2 = []
    p1 = []
    p2 = []
    ax=plt.subplot(3,1,i+1)
    for ls, pot, pl in zip(linestyle, potential, plab):
        wi = pwind(Gamma, M, potential=pot, expansion=ex, driver='ideal')
        p,=plt.plot(a, wi.U(x, a), 'k'+ls, lw=2)
        if i == 0 and pot == 'point':
            p1.append(p)
            lab1.append('Ideal')
        else:
            p2.append(p)
            lab2.append(pl)
        for clr, t in zip(color, tau):
            w = pwind(Gamma, M, potential=pot, expansion=ex,
                      driver='radiation', tau0=t)
            p,=plt.plot(a, w.U(x, a), clr+ls, lw=2)
            if i == 0 and pot == 'point':
                p1.append(p)
                lab1.append(r'Rad, $\tau_0 = {:3d}$'.format(int(t)))
        if pot == 'point':
            ymax = np.ceil(wi.U(x, a[-1]))
    plt.text(1.5, 0.8*ymax, exlab[i])
    plt.xlim([1,10])
    plt.ylim([0,ymax])
    if i == 0:
        plt.legend(p1, lab1, loc='lower right', ncol=1, prop={"size" : 10})
    if i != 0:
        plt.setp(ax.get_yticklabels()[-1], visible=False)
    if i == 1:
        plt.ylabel(r'$U_a(x)$')
        plt.legend(p2, lab2, loc='lower right', ncol=2, prop={"size" : 10})
    if i < 2:
        plt.gca().get_xaxis().set_ticklabels([])
plt.subplots_adjust(bottom=0.1, hspace=0, wspace=0, top=0.95, right=0.95)
plt.xlabel(r'$a$')
plt.savefig('acc_law_rad.pdf')

# Third plot: compare wind acceleration laws for the ideal, hot gas
# cases
Gamma = 0.1
M = 30.0
uh = [2., 5., 10.]
x = -3.0
plt.figure(3, figsize=(5,6))
plt.clf()
for i, ex in enumerate(expansion):
    lab1 = []
    lab2 = []
    p1 = []
    p2 = []
    ax=plt.subplot(3,1,i+1)
    for ls, pot, pl in zip(linestyle, potential, plab):
        wi = pwind(Gamma, M, potential=pot, expansion=ex, driver='ideal')
        p,=plt.plot(a, wi.U(x, a), 'k'+ls, lw=2)
        if i == 0 and pot == 'point':
            p1.append(p)
            lab1.append('Ideal')
        else:
            p2.append(p)
            lab2.append(pl)
        for clr, u in zip(color, uh):
            w = pwind(Gamma, M, potential=pot, expansion=ex,
                      driver='hot', uh=u)
            p,=plt.plot(a, w.U(x, a), clr+ls, lw=2)
            if i == 0 and pot == 'point':
                p1.append(p)
                lab1.append(r'Hot, $u_h = {:2d}$'.format(int(u)))
        if pot == 'point':
            ymax = np.ceil(wi.U(x, a[-1]))
    plt.text(1.5, 0.8*ymax, exlab[i])
    plt.xlim([1,10])
    plt.ylim([0,ymax])
    if i == 0:
        plt.legend(p1, lab1, loc='lower right', ncol=1, prop={"size" : 10})
    if i != 0:
        plt.setp(ax.get_yticklabels()[-1], visible=False)
    if i == 1:
        plt.ylabel(r'$U_a(x)$')
        plt.legend(p2, lab2, loc='lower right', ncol=2, prop={"size" : 10})
    if i < 2:
        plt.gca().get_xaxis().set_ticklabels([])
plt.subplots_adjust(bottom=0.1, hspace=0, wspace=0, top=0.95, right=0.95)
plt.xlabel(r'$a$')
plt.savefig('acc_law_hot.pdf')
