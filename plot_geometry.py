"""
This script produces an illustration of the geometry of the wind.
"""

import matplotlib
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.collections import PolyCollection
from despotic.winds import pwind

##################################################################
# This is much easier if we define some generic geometry functions
# first
##################################################################

def rotmat(vec):
    """
    Returns the rotation matrix that rotates a vector vec
    so that it lies along the z axis.
    
    Parameters:
        vec : array(3)
            input vector; need not be a unit vector
            
    Returns:
        rotmat : array(3,3)
            the rotation matrix that rotates vec to lie along
            the z axis
    """
    # Form unit vector
    nvec = vec/np.sqrt(np.sum(vec**2))
    nx = nvec[0]
    ny = nvec[1]
    nz = nvec[2]
    nxy = np.sqrt(nvec[0]**2 + nvec[1]**2)
    # Form rotation matrix
    if nxy > 0:
        rotmat = np.array([
                [(ny**2+nx**2*nz)/nxy**2, nx*ny*(nz-1)/nxy**2, -nx],
                [nx*ny*(nz-1)/nxy**2, (nx**2+ny**2*nz)/nxy**2, -ny],
                [nx, ny, nz]])
    else:
        rotmat = np.array(np.eye(3,3))
    return rotmat

def zrot(vec, x, y, z):
    """
    Given a vector and a set of coordinate arrays x, y, z, return
    x, y, and z rotated under a rotation such that vec lies along
    the z axis
    
    Parameters:
        vec : array(3)
            input vector; need not be a unit vector
        x, y, z : array
            coordinate arrays to be rotated; must all have the same
            shape
            
    Returns:
        xr, yr, zr : array
            rotated versions of x, y, and z
    """
    # Get rotation matrix
    rmat = rotmat(vec)
    # Rotate
    sh = x.shape
    xyzr = np.einsum('ij,ki', rmat, 
                     np.array(zip(x.flatten(), y.flatten(), z.flatten())))
    # Extract result and return
    xr = xyzr[0,:].reshape(sh)
    yr = xyzr[1,:].reshape(sh)
    zr = xyzr[2,:].reshape(sh)
    return xr, yr, zr

def rodrot(vec, theta):
    """
    This returns the rotation matrix for rotation about a vector vec
    by an angle theta, computed using Rodrigues' rotation formula
    
    Parameters:
        vec : array(3)
            vector about which to rotate
        theta : float
            rotation angle
            
    Returns:
        rotmat : array(3,3)
            rotation matrix
    """
    u = vec/np.sqrt(np.sum(vec**2))
    ux = u[0]
    uy = u[1]
    uz = u[2]
    c = np.cos(theta)
    s = np.sin(theta)
    rotmat = np.array([
            [c+ux**2*(1-c),    ux*uy*(1-c)-uz*s, ux*uz*(1-c)+uy*s],
            [uy*ux*(1-c)+uz*s, c+uy**2*(1-c),    uy*uz*(1-c)-ux*s],
            [uz*ux*(1-c)-uy*s, uz*uy*(1-c)+ux*s, c+uz**2*(1-c)]
        ])
    return rotmat

def boxclip(x, y, z, xlim, ylim, zlim):
    """
    Given a set of coordinates (x,y,z) and a set of box limits 
    (xlim, ylim, zlim), return versions of (x,y,z) with the values
    of points outside the box set to NaN
    
    Parameters:
        x, y, z : array
            input coordinates
        xlim, ylim, zlim : array(2)
            lower and upper coordinate limits for the clipping box
    
    Returns:
        xc, yc, zc : array
            clipped versions of x, y, z with points outside the
            clipping box replaced by NaN
    """
    idx = np.logical_or.reduce((
            x < xlim[0], x > xlim[1],
            y < ylim[0], y > ylim[1],
            z < zlim[0], z > zlim[1]))
    xc = np.copy(x)
    xc[idx] = np.nan
    yc = np.copy(y)
    yc[idx] = np.nan
    zc = np.copy(z)
    zc[idx] = np.nan
    return xc, yc, zc

def arc(vec1, vec2, n=50, r=1.0):
    """
    Given a pair of vectors with their bases at the origin, return
    a set of points forming an arc of constant radius between them,
    uniformly spaced in angle
    
    Parameters:
        vec1, vec2 : array(3)
            vectors between which arc is to be found
        n : int
            number of points in the arc
        r : float
            radius of the arc
            
    Returns:
        x, y, z : array
            3d (x,y,z) coordinates of the points that constitute
            the arc
    """
    # First step: take the cross product of the two input vectors to
    # find an orthogonal vector about which we can rotate
    avec = np.asarray(vec1)
    bvec = np.asarray(vec2)
    bvecn = bvec/np.sqrt(np.sum(bvec**2))
    ovec = np.array([avec[1]*bvec[2]-avec[2]*bvec[1],
                     avec[2]*bvec[0]-avec[0]*bvec[2],
                     avec[0]*bvec[1]-avec[1]*bvec[0]])
    ovec = ovec / np.sqrt(np.sum(ovec**2))
    # Second step: get the angle between the two vectors
    theta = np.arccos(np.sum(avec*bvec)/np.sqrt(np.sum(avec**2)*np.sum(bvec**2)))
    # Third step: construct arc, using Rodrigues's rotation formula
    x = np.zeros(n)
    y = np.zeros(n)
    z = np.zeros(n)
    for i, th in enumerate(np.linspace(0, theta, n)):
        pt = np.einsum('ij,i', rodrot(ovec, th), r*bvecn)
        x[i] = pt[0]
        y[i] = pt[1]
        z[i] = pt[2]
    # Return
    return np.array(x), np.array(y), np.array(z)


######################################################################
# Now we compute the coordinates for the geometric structures we want
# to plot
######################################################################

# Set up bounding box for plot
xlim = [-3,3]
ylim = [-3,3]
zlim = [0,6]

# Set up sphere
npts = 2000
x = np.linspace(-1, 1, npts)
xxs, yys = np.meshgrid(x, x)
rho = np.sqrt(xxs**2 + yys**2)
zs = np.sqrt(1.0 - np.minimum(rho, 1.0)**2)
zs[zs == 0.0] = np.nan

# Set up example line of sight
varpi = 1.2
varpi_t = -0.3

# Set up the outer cone

# Opening angle and direction
theta = np.pi/6
theta_in = np.pi/8
phi = 10/90.*np.pi/2
vec = np.array([np.sin(phi),0,np.cos(phi)])

# Make the cones with axis oriented along (1,0,0), then rotate
x = np.linspace(-6, 6, npts)
xxc, yyc = np.meshgrid(x, x)
xxci, yyci = np.meshgrid(x, x)
zc = np.sqrt(xxc**2+yyc**2)/np.tan(theta)
zci = np.sqrt(xxci**2+yyci**2)/np.tan(theta_in)
xxc, yyc, zc = zrot(vec, xxc, yyc, zc)
xxci, yyci, zci = zrot(vec, xxci, yyci, zci)

# Clip
xxc, yyc, zc = boxclip(xxc, yyc, zc, xlim, ylim, zlim)
zc[xxc**2 + yyc**2 + zc**2 < 1] = np.nan
xxci, yyci, zci = boxclip(xxci, yyci, zci, xlim, ylim, zlim)
zci[xxci**2 + yyci**2 + zci**2 < 1] = np.nan

# Central axis of cones
xca = np.zeros(100)
yca = np.zeros(100)
zca = np.linspace(1, 10, 100)
xca, yca, zca = zrot(vec, xca, yca, zca)

# Line along cone to illustrate opening angle
xcs = np.linspace(0, 10, 100)
ycs = np.zeros(100)
zcs = xcs/np.tan(theta)
xcs, ycs, zcs = zrot(vec, xcs, ycs, zcs)
zcsi = xcs/np.tan(theta_in)
xcsi, ycsi, zcsi = zrot(vec, xcs, ycs, zcsi)

# Clip
xca, yca, zca = boxclip(xca, yca, zca, xlim, ylim, zlim)
xcs, ycs, zcs = boxclip(xcs, ycs, zcs, xlim, ylim, zlim)
zcs[xcs**2+ycs**2+zcs**2 < 1] = np.nan


######################################################################
# Make a plot looking edge-on
######################################################################

# Set up axis
fig = plt.figure(figsize=plt.figaspect(1))
fig.clf()
ax = fig.add_subplot(111, projection='3d')

# Plot outer cone, inner cone, and sphere
ax.plot_surface(xxc, yyc, zc, color='b', alpha=0.25, linewidth=0)
ax.plot_surface(xxci, yyci, zci, color='r', alpha=0.25, linewidth=0)
ax.plot_surface(xxs, yys, zs, color='k', alpha=0.25, linewidth=0)

# Plot cone axis, vertical line
ax.plot(xca, yca, zca, color='k', linewidth=1, linestyle='-')
ax.plot([0,0], [0,0], [1, zlim[1]], color='k', linewidth=1, linestyle='--')

# Plot arcs and add labels
idx = np.argmax(np.logical_and.reduce(
        (np.isfinite(xcs), np.isfinite(ycs), np.isfinite(zcs))))
xa, ya, za = arc([xcs[idx], ycs[idx], zcs[idx]], vec, r=2.5)
ax.plot(xa, ya, za, color='k', linewidth=1)
ax.text(xa[25]*1.15, ya[25]*1.15, za[25]*1.15, r'$\theta_{\mathrm{out}}$',
       verticalalignment='center', horizontalalignment='center')
xa, ya, za = arc([xcsi[-1], ycsi[-1], zcsi[-1]], vec, r=3.5)
ax.plot(xa, ya, za, color='k', linewidth=1)
ax.text(xa[25]*1.15, ya[25]*1.15, za[25]*1.15, r'$\theta_{\mathrm{in}}$',
       verticalalignment='center', horizontalalignment='center')
xa, ya, za = arc([0,0,1], vec, r=2)
ax.plot(xa, ya, za, color='k', linewidth=1)
ax.text(xa[25]*1.15, ya[25]*1.15, za[25]*1.15, r'$\phi$',
       verticalalignment='center', horizontalalignment='center')

# Add arrow pointing toward the Sun
ax.quiver([-2.5], [0], [5], [-1], [0], [0], color='k')
ax.text(-2.0, 0, 4.8, 'To Earth', 
        horizontalalignment='center',
        verticalalignment='top')

# Add example line of sight; use pwind structure to compute entry and
# exit points for us
w = pwind(0.1, 30, geometry='cone sheath', phi=phi, theta=theta,
          theta_in=theta_in)
s = w.s_crit(varpi, varpi_t)
s = np.insert(s, 0, xlim[0])
s = np.append(s, xlim[1])
for i in range(s.size/2):
    ax.plot([s[2*i], s[2*i+1]], [varpi_t,varpi_t], [varpi,varpi], linewidth=1,
            color='k', linestyle=':')
    if i < s.size/2-1:
        ax.plot([s[2*i+1], s[2*i+2]], [varpi_t,varpi_t], [varpi,varpi],
                linewidth=1,
                color='k', linestyle='-')
ax.text(-2.3, varpi_t, varpi+0.05,
        r'$(\varpi_a, \varpi_t) = ({:3.1f}, {:3.1f})$'.
        format(varpi, varpi_t),
        horizontalalignment='center',
        verticalalignment='bottom')

# Set bounding box
ax.set_xlim3d(xlim)
ax.set_ylim3d(ylim)
ax.set_zlim3d(zlim)

# Add labels
ax.set_xlabel(r'$s$')
ax.set_yticks([])
ax.set_zlabel(r'$\varpi_a$')

# Move camera
ax.view_init(elev = 0, azim=-90)
ax.dist = 7.5

# Save
plt.savefig('geometry_edge.pdf')

######################################################################
# Make a plot from another perspective
######################################################################

# Set up axis
fig = plt.figure(figsize=plt.figaspect(1))
fig.clf()
ax = fig.add_subplot(111, projection='3d')

# Plot outer cone, inner cone, and sphere
ax.plot_surface(xxc, yyc, zc, color='b', alpha=0.25, linewidth=0)
ax.plot_surface(xxci, yyci, zci, color='r', alpha=0.25, linewidth=0)
ax.plot_surface(xxs, yys, zs, color='k', alpha=0.25, linewidth=0)

# Plot cone axis, vertical line
ax.plot(xca, yca, zca, color='k', linewidth=1, linestyle='-')
ax.plot([0,0], [0,0], [1, zlim[1]], color='k', linewidth=1, linestyle='--')

# Plot arcs and add labels
idx = np.argmax(np.logical_and.reduce(
        (np.isfinite(xcs), np.isfinite(ycs), np.isfinite(zcs))))
xa, ya, za = arc([xcs[idx], ycs[idx], zcs[idx]], vec, r=2.5)
ax.plot(xa, ya, za, color='k', linewidth=1)
ax.text(xa[25]*1.15, ya[25]*1.15, za[25]*1.15, r'$\theta_{\mathrm{out}}$',
       verticalalignment='center', horizontalalignment='center')
xa, ya, za = arc([xcsi[-1], ycsi[-1], zcsi[-1]], vec, r=3.5)
ax.plot(xa, ya, za, color='k', linewidth=1)
ax.text(xa[25]*1.15, ya[25]*1.15, za[25]*1.15, r'$\theta_{\mathrm{in}}$',
       verticalalignment='center', horizontalalignment='center')
xa, ya, za = arc([0,0,1], vec, r=2)
ax.plot(xa, ya, za, color='k', linewidth=1)
ax.text(xa[25]*1.15, ya[25]*1.15, za[25]*1.15, r'$\phi$',
       verticalalignment='center', horizontalalignment='center')

# Add arrow pointing toward the Sun
ax.quiver([-2.5], [0], [5], [-1], [0], [0], color='k')
ax.text(-2.0, 0, 4.8, 'To Earth', 
        horizontalalignment='center',
        verticalalignment='top')

# Add example line of sight; use pwind structure to compute entry and
# exit points for us
w = pwind(0.1, 30, geometry='cone sheath', phi=phi, theta=theta,
          theta_in=theta_in)
s = w.s_crit(varpi, varpi_t)
s = np.insert(s, 0, xlim[0])
s = np.append(s, xlim[1])
for i in range(s.size/2):
    ax.plot([s[2*i], s[2*i+1]], [varpi_t,varpi_t], [varpi,varpi], linewidth=1,
            color='k', linestyle=':')
    if i < s.size/2-1:
        ax.plot([s[2*i+1], s[2*i+2]], [varpi_t,varpi_t], [varpi,varpi],
                linewidth=1,
                color='k', linestyle='-')
ax.text(-2.3, varpi_t, varpi+0.05,
        r'$(\varpi_a, \varpi_t) = ({:3.1f}, {:3.1f})$'.
        format(varpi, varpi_t),
        horizontalalignment='center',
        verticalalignment='bottom')

# Set bounding box
ax.set_xlim3d(xlim)
ax.set_ylim3d(ylim)
ax.set_zlim3d(zlim)

# Add labels
ax.set_xlabel(r'$s$')
ax.set_ylabel(r'$\varpi_t$')
ax.set_zlabel(r'$\varpi_a$')

# Move camera
ax.dist = 9.4

# Save
plt.savefig('geometry_perspective.pdf')
