"""
This script make a plot showing the momentum flux at infinity from
winds of various drivers and geometries.
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colorbar as cbar
import matplotlib.colors as colors
from despotic.winds import pwind

# Get momentum flux for ideal wind with constant area
Gamma = np.logspace(-2., 0, 100)
mach = np.logspace(0.5, 2.5, 100)
pdot = np.zeros((Gamma.size, mach.size))
for i, g in enumerate(Gamma):
    for j, m in enumerate(mach):
        w = pwind(g, m, driver='ideal', expansion='area',
                  potential='point')
        pdot[i,j] = w.pdot(1e10)

# Get momentum flux for radiatively-driven winds with various expansions
tau0 = np.logspace(1., 3., 50.)
m = 100.0
expansion = np.array(['area', 'intermediate', 'solid angle'])
exlab = ['Area', 'Int', 'SA']
pdotrad = np.zeros((expansion.size, Gamma.size, tau0.size))
for j, ex in enumerate(expansion):
    for i, g in enumerate(Gamma):
        for k, t in enumerate(tau0):
            if g*t > 1:
                w = pwind(g, m, tau0=t, driver='radiation',
                          expansion=ex,
                          potential='point')
                pdotrad[j,i,k] = w.pdot(1e10)


# Plot
fig = plt.figure(1, figsize=(4,6))
plt.clf()
ymar = [0.1, 0.95]
xmar = [0.18, 0.72]
xgap = 0.03
ygap = 0.05
dx = xmar[1] - xmar[0]
dy = (ymar[1]-ymar[0]-ygap)/4.
clev = np.array([-2,-1,-0.5,0, 0.5])
vmin = -3
vmax = 1

# Ideal case
rect = (xmar[0], ymar[0]+ygap+3*dy, dx, dy)
fig.add_axes(rect)
plt.imshow(np.transpose(np.log10(pdot)),
           extent=[np.log10(Gamma[0]), np.log10(Gamma[-1]),
                   np.log10(mach[0]), np.log10(mach[-1])],
           origin='lower', aspect='auto',
           vmin=vmin, vmax=vmax)
cs = plt.contour(np.log10(Gamma), np.log10(mach),
                 np.transpose(np.log10(pdot)),
                 levels=clev, colors='k',
                 linewidths=2)
plt.clabel(cs, inline=1, fmt='$%3.1f$')
plt.ylabel(r'$\log\,\mathcal{M}$')
plt.gca().xaxis.set_ticklabels([])
plt.text(-0.5, 0.5+0.7*(2.5-0.5), 'Ideal\nArea', size=10)

for i in range(3):
    rect = (xmar[0], ymar[0]+(2-i)*dy, dx, dy)
    ax = fig.add_axes(rect)
    plt.imshow(np.transpose(np.log10(pdotrad[i,:,:]+1e-50)),
               extent=[np.log10(Gamma[0]), np.log10(Gamma[-1]),
                       np.log10(tau0[0]), np.log10(tau0[-1])],
               origin='lower', aspect='auto',
               vmin=vmin, vmax=vmax)
    plt.fill_between(np.log10(Gamma), np.log10(1.0/Gamma), color='#bbbbbb')
    plt.ylim(np.log10([tau0[0], tau0[-1]]))
    cs = plt.contour(np.log10(Gamma), np.log10(tau0),
                     np.transpose(np.log10(pdotrad[i,:,:]+1e-50)),
                     levels=clev, colors='k',
                     linewidths=2, hatches=['', '', '', '', '//'])
    plt.contourf(np.log10(Gamma), np.log10(tau0),
                 np.transpose(np.log10(pdotrad[i,:,:]+1e-50)),
                 levels=[0,1e6], colors='k', alpha=0.25,
                 linewidths=2)
    plt.clabel(cs, inline=1, fmt='$%3.1f$')
    plt.text(-1.9, 1.15, 'Rad\n'+exlab[i], size=10)
    if i != 0:
        plt.setp(ax.get_yticklabels()[-1], visible=False)
    if i == 1:
        plt.ylabel(r'$\log\,\tau_0$')
    if i == 2:
        plt.xlabel(r'$\log\,\Gamma$')
    else:
        ax.xaxis.set_ticklabels([])

# Add color bar
rect = (xmar[1]+xgap, ymar[0], 0.03, ymar[1]-ymar[0])
cax = fig.add_axes(rect)
norm = colors.Normalize(vmin=vmin, vmax=vmax)
cb = cbar.ColorbarBase(cax, norm=norm,
                       label=r'$\log\,\dot{p}_{\mathrm{sh}}(\infty)/\dot{p}$')

# Save
plt.savefig('momentum.pdf')
