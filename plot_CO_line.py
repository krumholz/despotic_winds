"""
This script plots examples of CO line emission.
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as colors
from despotic import emitterData, emitter
from despotic.winds import pwind
import scipy.constants as physcons
me = physcons.m_e * 1e3
mH = physcons.m_p * 1e3 + me

# Gamma and Mach
Gamma = 0.2
Mach = 50.0

# Set up winds
wa = pwind(Gamma, Mach, expansion='area', potential='isothermal')
ws = pwind(Gamma, Mach, expansion='solid angle', potential='isothermal')

# Grab CO data
co = emitter('co', 1.1e-4)
mX = 1.4*mH / co.abundance
tX = co.tX(mX, trans=0)

# Grid of tw values
tXtw = np.logspace(-1,2,7)
tw = tX/tXtw

# Grid in u
u = np.linspace(-3, 3, 100)

# Gas kinetic temperature
T = 50.0

# Plot
plt.figure(1, figsize=(6,6))
plt.clf()
norm = colors.Normalize(vmin=-0.5, vmax=1)
cmap = cm.ScalarMappable(norm=norm, cmap=cm.get_cmap('Blues'))
for i in range(len(tXtw)):

    # Get rgb value for this tXtw
    rgb = cmap.to_rgba(float(i)/(len(tXtw)-1))

    # Compute brightness temperatures
    T_c_a = wa.temp_LTE(u, T, tw=tw[i], emit=co, correlated=True)
    T_uc_a = wa.temp_LTE(u, T, tw=tw[i], emit=co, correlated=False)
    T_c_s = ws.temp_LTE(u, T, tw=tw[i], emit=co, correlated=True)
    T_uc_s = ws.temp_LTE(u, T, tw=tw[i], emit=co, correlated=False)

    # Compute brightness temperatures at varpi=2
    T_c_a_vp2 = wa.temp_LTE(u, T, tw=tw[i], emit=co, varpi=2,
                            correlated=True)
    T_uc_a_vp2 = wa.temp_LTE(u, T, tw=tw[i], emit=co, varpi=2,
                             correlated=False)
    T_c_s_vp2 = ws.temp_LTE(u, T, tw=tw[i], emit=co, varpi=2,
                            correlated=True)
    T_uc_s_vp2 = ws.temp_LTE(u, T, tw=tw[i], emit=co, varpi=2,
                             correlated=False)

    # Plot
    plt.subplot(2,2,1)
    p1,=plt.plot(-u, np.log10(T_c_a/T), linestyle='-',
                 color=rgb, lw=2)
    p2,=plt.plot(-u, np.log10(T_uc_a/T), linestyle='--',
                 color=rgb, lw=2)
    if i == len(tXtw)-1:
        plt.legend([p1,p2], ['Correlated', 'Uncorrelated'],
                   loc='lower left', prop={'size':10})

    plt.subplot(2,2,2)
    plt.plot(-u, np.log10(T_c_s/T), ls='-', color=rgb, lw=2)
    plt.plot(-u, np.log10(T_uc_s/T), ls='--', color=rgb, lw=2)

    # Plot at varpi = 2
    plt.subplot(2,2,3)
    p1,=plt.plot(-u, np.log10(T_c_a_vp2/T), linestyle='-',
                 color=rgb, lw=2)
    p2,=plt.plot(-u, np.log10(T_uc_a_vp2/T), linestyle='--',
                 color=rgb, lw=2)

    plt.subplot(2,2,4)
    plt.plot(-u, np.log10(T_c_s_vp2/T), ls='-', color=rgb, lw=2)
    plt.plot(-u, np.log10(T_uc_s_vp2/T), ls='--', color=rgb, lw=2)

# Tweak plots
plt.subplot(2,2,1)
plt.ylim([-4,0])
plt.title('Area')
plt.ylabel(r'$\log\,T_B/T$')
plt.setp(plt.gca().get_yticklabels()[0], visible=False)
plt.gca().set_xticklabels([])

plt.subplot(2,2,2)
plt.ylim([-4,0])
plt.title('Solid Angle')
plt.gca().set_xticklabels([])
plt.gca().set_yticklabels([])
plt.twinx()
plt.gca().set_yticklabels([])
plt.ylabel(r'$\varpi = 0$')

plt.subplot(2,2,3)
plt.ylim([-4,0])
plt.xlabel(r'$u$')
plt.ylabel(r'$\log\,T_B/T$')

plt.subplot(2,2,4)
plt.ylim([-4,0])
plt.xlabel(r'$u$')
plt.setp(plt.gca().get_xticklabels()[0], visible=False)
plt.gca().set_yticklabels([])
plt.twinx()
plt.gca().set_yticklabels([])
plt.ylabel(r'$\varpi = 2$')

plt.subplots_adjust(wspace=0, hspace=0, left=0.15, right=0.90)

plt.savefig('CO_line.pdf')
