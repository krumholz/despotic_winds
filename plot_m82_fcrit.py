"""
Code to produce plots exploring the effects of varying f_crit
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import brentq
import matplotlib.cm as cm
import matplotlib.colors as colors
from despotic import emitter
from despotic.winds import pwind, zetaM, sxMach

# Constants; switch to cgs
from scipy.constants import G, c, m_p, m_e, h
from scipy.constants import k as kB
from astropy.units import Msun, yr, Angstrom, pc
c = 1e2*c
hP = h*1e7
G = 1e3*G
kB = 1e7*kB
mH = (m_e + m_p)*1e3
muH = 1.4
Msun = Msun.to('g')
yr = yr.to('s')
Myr = 1e6*yr
Gyr = 1e9*yr
ang = Angstrom.to('cm')
pc = pc.to('cm')
kpc = 1e3*pc

# Fiducial parameters
mdotstar = 4.1*Msun/yr
epsff = 0.01
mach = 100.0
uh = 10.0
v0 = 120e5*np.sqrt(2)
r0 = 250*pc
m0 = v0**2*r0/(2.0*G)
rho0 = 3.0*m0/(4.0*np.pi*r0**3)
tc = r0/v0
temp = 50.
dist = 3.5e3*kpc
mdot = 100*Msun/yr
tw = m0/mdot

# Opening angle and inclination
phi = -5.0/90.0*np.pi/2.0
theta_in = 30.0/90.0*np.pi/2.0
theta_out = 50./90.*np.pi/2.0

# f_crit values
f_crit = np.logspace(-1, 0, 6)

# Expansion types
expansion = ['area', 'intermediate', 'solid angle']

# Winds
if 1:
    pw = []
    for fc in f_crit:

        # Derived parameters
        eta = (np.cos(theta_in) - np.cos(theta_out)) * mdot / mdotstar
        Gamma = brentq(
            lambda g: zetaM(np.log(g)+np.log(fc), sxMach(mach))/epsff - eta,
            1e-6, 100.)

        pw1 = []
        for ex in expansion:
            pw1.append(
                pwind(Gamma, mach, driver='hot', potential='isothermal',
                      expansion=ex, geometry='cone sheath',
                      theta=theta_out, theta_in=theta_in, phi=phi,
                      uh=uh, fcrit=fc, interpabs=5e-3, interprel=5e-3))
        pw.append(pw1)

    # Position at which to plot
    varpi_a = kpc/r0
    varpi_t = 0.0

    # Velocity grid
    u = np.linspace(-3, 3, 100)

    #######################################################################
    # Halpha emission
    #######################################################################

    # Cooling constant
    lam_e = 3.9e-25

    # Wavelength grid
    lam0 = 6562.801*ang
    lam = lam0*(1.0+u*v0/c)

    # Ha emission profiles
    Ha = np.empty((len(f_crit), len(expansion), len(u)))
    for i, pw1 in enumerate(pw):
        for j, pw2 in enumerate(pw1):
            Ha[i,j,:] = 1e17*2.0/(36.*np.pi) * lam_e * r0 * \
                        (rho0/mH)**2 * lam0 / v0 * \
                        (tc/tw)**2 * pw2.Xi(u, varpi=varpi_a)

# Plot
fig = plt.figure(1, figsize=(4,6))
plt.clf()
norm = colors.Normalize(vmin=-0.5, vmax=1)
cmap = cm.ScalarMappable(norm=norm, cmap=cm.get_cmap('Blues'))
for i, pw1 in enumerate(pw):
    for j, pw2 in enumerate(pw1):

        # Get rgb value
        rgb = cmap.to_rgba(float(i)/(len(f_crit)-1))

        # Plot
        plt.subplot(len(expansion), 1, 1+j)
        #plt.plot(lam*1e7, np.log10(Ha[i,j,:]+1e-30), color=rgb, lw=2)
        plt.plot(u*v0/1e7, np.log10(Ha[i,j,:]+1e-30), color=rgb, lw=2)
        #plt.xlim([654.5, 658])
        plt.xlim([-5,5])
        plt.ylim([-2,3])
        #plt.plot(lam0*(1.0+np.ones(2)*v0/c)*1e7, [-2, 3], 'k:')
        #plt.plot(lam0*(1.0-np.ones(2)*v0/c)*1e7, [-2, 3], 'k:')
        plt.plot(np.ones(2)*v0/1e7, [-2, 3], 'k:')
        plt.plot(-np.ones(2)*v0/1e7, [-2, 3], 'k:')

        # Adjust axes
        if j != len(expansion)-1:
            plt.gca().set_xticklabels([])
        else:
            #plt.gca().set_xticks([655, 656, 657])
            #plt.xlabel(r'$\lambda$ [nm]')
            plt.xlabel(r'$v$ [10$^2$ km s$^{-1}$]')
        if j == 0:
            ax = plt.gca().twiny()
            #ax.set_xlim(np.array([u[0]*v0, u[-1]*v0])/1e7)
            #ax.set_xlabel(r'$v$ [10$^2$ km s$^{-1}$]')
            #ax.set_xticks([-8, -4, 0, 4, 8])
            ax.set_xlim(lam0*1e7*(1.0+np.array([-5,5])*v0/c))
            ax.set_xticks([655, 656, 657, 658])
            ax.set_xticklabels(ax.xaxis.get_majorticklabels(), rotation=90)
            ax.set_xlabel(r'$\lambda$ [nm]')
        if j != 0:
            plt.gca().set_yticks([-2, -1, 0, 1, 2])
        if j == 1:
            plt.ylabel(r'$\log \,I_\nu$ [MJy sr$^{-1}$]')
        ax = plt.gca().twinx()
        ax.set_ylabel(expansion[j].title())
        ax.set_yticks([])
plt.subplots_adjust(hspace=0, wspace=0, left=0.16, top=0.9)
plt.savefig('m82_Ha_fcrit.pdf')

#######################################################################
# CO emission
#######################################################################

# Import the required molecular data
co = emitter('CO', 1.1e-4)

# CO emission profiles
CO_TB = np.empty((len(f_crit), len(expansion), len(u)))
for i, pw1 in enumerate(pw):
    for j, pw2 in enumerate(pw1):
        CO_TB[i,j,:] = pw2.temp_LTE(u, temp, emit=co, tw=tw,
                                    varpi=varpi_a)

# Plot
fig = plt.figure(2, figsize=(4,6))
plt.clf()
norm = colors.Normalize(vmin=-0.5, vmax=1)
cmap = cm.ScalarMappable(norm=norm, cmap=cm.get_cmap('Blues'))
for i, pw1 in enumerate(pw):
    for j, pw2 in enumerate(pw1):

        # Get rgb value
        rgb = cmap.to_rgba(float(i)/(len(f_crit)-1))

        # Plot
        plt.subplot(len(expansion), 1, 1+j)
        plt.plot(u*v0/1e7, np.log10(CO_TB[i,j,:]+1e-30), color=rgb, lw=2)
        plt.xlim([-5,5])
        plt.ylim([-4,0])
        plt.plot(np.ones(2)*v0/1e7, [-4, 0], 'k:')
        plt.plot(-np.ones(2)*v0/1e7, [-4, 0], 'k:')

        # Adjust axes
        if j != len(expansion)-1:
            plt.gca().set_xticklabels([])
        else:
            plt.xlabel(r'$v$ [10$^2$ km s$^{-1}$]')
        if j != 0:
            plt.gca().set_yticks([-4, -3.5, -3, -2.5, -2, -1.5, -1, -0.5])
        if j == 1:
            plt.ylabel(r'$\log \,T_B$ [K]')
        ax = plt.gca().twinx()
        ax.set_ylabel(expansion[j].title())
        ax.set_yticks([])
plt.subplots_adjust(hspace=0, wspace=0, left=0.18, top=0.95)
plt.savefig('m82_CO_fcrit.pdf')
       
