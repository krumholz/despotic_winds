"""
Code to produce plots for variations on fiducial M82 example
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import brentq
import matplotlib.cm as cm
import matplotlib.colors as colors
from despotic import emitter
from despotic.winds import pwind, zetaM, sxMach

# Constants; switch to cgs
from scipy.constants import G, c, m_p, m_e, h
from scipy.constants import k as kB
from astropy.units import Msun, yr, Angstrom, pc
c = 1e2*c
hP = h*1e7
G = 1e3*G
kB = 1e7*kB
mH = (m_e + m_p)*1e3
muH = 1.4
Msun = Msun.to('g')
yr = yr.to('s')
Myr = 1e6*yr
Gyr = 1e9*yr
ang = Angstrom.to('cm')
pc = pc.to('cm')
kpc = 1e3*pc

# Fiducial parameters
mdotstar = 4.1*Msun/yr
epsff = 0.01
mach = 100.0
uh = 10.0
v0 = 120e5*np.sqrt(2)
r0 = 250*pc
m0 = v0**2*r0/(2.0*G)
rho0 = 3.0*m0/(4.0*np.pi*r0**3)
tc = r0/v0
temp = 50.
dist = 3.5e3*kpc

# Opening angle and inclination
phi = -5.0/90.0*np.pi/2.0
theta_in = 30.0/90.0*np.pi/2.0
theta_out = 50./90.*np.pi/2.0

# Varying parameters
mdot = np.logspace(1,3,7)*Msun/yr

if True:

    # Wind objects
    pw = []
    expansion = ['area', 'intermediate', 'solid angle']
    pot = [ 'point', 'isothermal']
    for i, md in enumerate(mdot):
        eta = (np.cos(theta_in) - np.cos(theta_out)) * md / mdotstar
        Gamma = brentq(
            lambda g: zetaM(np.log(g), sxMach(mach))/epsff - eta,
            1e-6, 100.)
        pw1 = []
        for j, ex in enumerate(expansion):
            pw2 = []
            for k, p in enumerate(pot):
                pw2.append(pwind(Gamma, mach, driver='hot', potential=p,
                                 expansion=ex, geometry='cone sheath',
                                 theta=theta_out, theta_in=theta_in,
                                 phi=phi, uh=uh, interpabs=5e-3,
                                 interprel=5e-3))
            pw1.append(pw2)
        pw.append(pw1)

    # Position at which to plot
    varpi_a = kpc/r0
    varpi_t = 0.0

    # Velocity grid
    u = np.linspace(-3, 3, 100)

#######################################################################
# Halpha emission
#######################################################################

# Cooling constant
lam_e = 3.9e-25

# Wavelength grid
lam0 = 6562.801*ang
lam = lam0*(1.0+u*v0/c)

# Ha emission profiles
Ha = np.empty((len(mdot), len(expansion), len(pot), len(u)))
for i, pw1 in enumerate(pw):
    for j, pw2 in enumerate(pw1):
        for k, pw3 in enumerate(pw2):
            tw = m0 / mdot[i]
            print 'Ha', i, j, k, pw3.Gamma, pw3.mach, \
                pw3.potential, pw3.expansion
            Ha[i,j,k] = 1e17*2.0/(36.*np.pi) * lam_e * r0 * \
                        (rho0/mH)**2 * lam0 / v0 * \
                        (tc/tw)**2 * pw3.Xi(u, varpi=varpi_a)

# Plot
fig = plt.figure(1, figsize=(4,6))
plt.clf()
norm = colors.Normalize(vmin=-0.5, vmax=1)
cmap = cm.ScalarMappable(norm=norm, cmap=cm.get_cmap('Blues'))
for i, pw1 in enumerate(pw):
    for j, pw2 in enumerate(pw1):
        for k, pw3 in enumerate(pw2):

            # Get rgb value for this tw
            rgb = cmap.to_rgba(float(i)/(len(mdot)-1))

            plt.subplot(len(expansion), len(pot), 1+k+len(pot)*j)
            plt.plot(u*v0/1e7, np.log10(Ha[i,j,k,:]+1e-30), color=rgb, lw=2)
            plt.plot(np.ones(2)*v0/1e7, [-2, 3], 'k:')
            plt.plot(-np.ones(2)*v0/1e7, [-2, 3], 'k:')
            if i == len(pw)-1:
                plt.plot(u*v0/1e7, np.log10(Ha[len(mdot)/2, -1, -1]+1e-30),
                         'r--')
            plt.xlim([-5,5])
            plt.ylim([-2,3])
            if j != len(expansion)-1:
                plt.gca().set_xticklabels([])
            else:
                #plt.gca().set_xticks([655, 656, 657])
                #plt.xlabel(r'$\lambda$ [nm]')
                plt.xlabel(r'$v$ [10$^2$ km s$^{-1}$]')
            if k == 0:
                if j != 0:
                    plt.gca().set_yticks([-2, -1, 0, 1, 2])
                if j == 1:
                    plt.ylabel(r'$\log \,I_\nu$ [MJy sr$^{-1}$]')
            else:
                plt.gca().set_yticklabels([])
            if j == 0:
                ax = plt.gca().twiny()
                plt.pause(0.01)   # Sidestep matplotlib bug
                ax.set_xlim(lam0*1e7*(1.0+np.array([-5,5])*v0/c))
                plt.pause(0.01)
                ax.set_xticks([655, 656, 657, 658])
                plt.pause(0.01)
                ax.set_xticklabels(ax.xaxis.get_majorticklabels(),
                                   rotation=90)
                plt.pause(0.01)
                ax.set_xlabel(r'$\lambda$ [nm]')
                plt.pause(0.01)
                ax.set_title(pot[k].title(), y=1.4)
            if k == len(pot)-1:
                ax = plt.gca().twinx()
                ax.set_ylabel(expansion[j].title())
                ax.set_yticks([])
plt.subplots_adjust(hspace=0, wspace=0, left=0.16, top=0.85)
plt.savefig('m82_Ha_var.pdf')

#######################################################################
# CO emission
#######################################################################

# Import the required molecular data
co = emitter('CO', 1.1e-4)

# CO emission profiles
CO_TB = np.empty((len(mdot), len(expansion), len(pot), len(u)))
for i, pw1 in enumerate(pw):
    tw = m0 / mdot[i]
    for j, pw2 in enumerate(pw1):
        for k, pw3 in enumerate(pw2):
            print 'CO', i, j, k
            CO_TB[i,j,k] = pw3.temp_LTE(u, temp, emit=co, tw=tw,
                                        varpi=varpi_a)

# Plot
fig = plt.figure(2, figsize=(4,6))
plt.clf()
norm = colors.Normalize(vmin=-0.5, vmax=1)
cmap = cm.ScalarMappable(norm=norm, cmap=cm.get_cmap('Blues'))
for i, pw1 in enumerate(pw):
    for j, pw2 in enumerate(pw1):
        for k, pw3 in enumerate(pw2):

            # Get rgb value for this tXtw
            rgb = cmap.to_rgba(float(i)/(len(mdot)-1))

            plt.subplot(len(expansion), len(pot), 1+k+len(pot)*j)
            plt.plot(u*v0/1e7, np.log10(CO_TB[i,j,k,:]+1e-30), color=rgb, lw=2)
            plt.plot(np.ones(2)*v0/1e7, [-4, 0.5], 'k:')
            plt.plot(-np.ones(2)*v0/1e7, [-4, 0.5], 'k:')
            if i == len(pw)-1:
                plt.plot(u*v0/1e7,
                         np.log10(CO_TB[len(mdot)/2, -1, -1]+1e-30),
                         'r--')
            plt.xlim([-5, 5])
            plt.ylim([-3, 0.5])
            if j != len(expansion)-1:
                plt.gca().set_xticklabels([])
            else:
                #plt.gca().set_xticks([-6, -3, 0, 3])
                plt.xlabel(r'$v$ [$10^2$ km s$^{-1}$]')
            if k == 0:
                #if j != 0:
                plt.gca().set_yticks([-3, -2, -1, 0])
                if j == 1:
                    plt.ylabel(r'$\log\, T_B$ [K]')
            else:
                plt.gca().set_yticklabels([])
            if j == 0:
                plt.title(pot[k].title(), y=1)
            if k == len(pot)-1:
                ax = plt.gca().twinx()
                ax.set_ylabel(expansion[j].title())
                ax.set_yticks([])
plt.subplots_adjust(hspace=0, wspace=0, left=0.2, top=0.9)
plt.savefig('m82_CO_var.pdf')


#######################################################################
# MgII, NaI absorption
#######################################################################

# Absorption lines
lineid = [r'Mg II $\lambda\lambda 2796, 2804$',
          r'Na I $\lambda\lambda 5892, 5898$']
lam0 = [[279.6, 280.4], [589.2, 589.8]]
u_trans = [[0, (lam0[0][1]-lam0[0][0])/lam0[0][0]*c/v0],
           [0, (lam0[1][1]-lam0[1][0])/lam0[1][0]*c/v0]]
tX = np.array([[190., 95.], [0.32, 0.16]])*Gyr

# Wavelength grids
nlam = 400
lam = [np.linspace(278., 282., nlam), np.linspace(587., 592., nlam)]

# Compute absorption
tau = np.zeros((len(mdot), len(expansion), len(pot), 2, nlam))
for i, pw1 in enumerate(pw):
    tw = m0 / mdot[i]
    for j, pw2 in enumerate(pw1):
        for k, pw3 in enumerate(pw2):
            for l, l0 in enumerate(lam0):
                u = (lam[l]/l0[0]-1.0)*c/v0
                tau[i,j,k,l,:] = pw3.tau_c(u, tXtw=tX[l]/tw,
                                           u_trans=u_trans[l],
                                           varpi=varpi_a)

# Plot MgII
fig = plt.figure(3, figsize=(4,6))
plt.clf()
norm = colors.Normalize(vmin=-0.5, vmax=1)
cmap = cm.ScalarMappable(norm=norm, cmap=cm.get_cmap('Blues'))
for i, pw1 in enumerate(pw):
    for j, pw2 in enumerate(pw1):
        for k, pw3 in enumerate(pw2):

            # Get rgb value for this tXtw
            rgb = cmap.to_rgba(float(i)/(len(mdot)-1))

            plt.subplot(len(expansion), len(pot), 1+k+len(pot)*j)
            plt.plot(lam[0], np.exp(-tau[i,j,k,0,:]), color=rgb, lw=2)
            if i == len(pw)-1:
                plt.plot(lam0[0][0]*np.ones(2), [0, 1], 'k:')
                plt.plot(lam0[0][1]*np.ones(2), [0, 1], 'k:')
            plt.xlim([278, 282])
            if j == 0:
                plt.ylim([0.9, 1])
            elif j == 1:
                plt.ylim([0.7, 1])
                plt.gca().set_yticks([0.7, 0.75, 0.80, 0.85, 0.9, 0.95])
            else:
                plt.ylim([0,1])
                plt.gca().set_yticks([0, 0.2, 0.4, 0.6, 0.8])
            if j != len(expansion)-1:
                plt.gca().set_xticklabels([])
            else:
                if k == 0:
                    plt.gca().set_xticks([278, 279, 280, 281])
                else:
                    plt.gca().set_xticks([278, 279, 280, 281, 282])
                plt.xlabel(r'$\lambda$ [nm]')
            if k == 0:
                 if j == 1:
                    plt.ylabel(r'Transmission fraction')
            else:
                plt.gca().set_yticklabels([])
            if j == 0:
                ax = plt.gca().twiny()
                ax.set_xlim([c*(lam[0][0]/lam0[0][0]-1)/1e8,
                             c*(lam[0][-1]/lam0[0][0]-1)/1e8])
                ax.set_xlabel(r'$v$ [10$^3$ km s$^{-1}$]')
                ax.set_title(pot[k].title(), y=1.3)
            if k == len(pot)-1:
                ax = plt.gca().twinx()
                ax.set_ylabel(expansion[j].title())
                ax.set_yticks([])
plt.subplots_adjust(hspace=0, wspace=0, left=0.2, top=0.85)
plt.savefig('m82_MgII_var.pdf')


# Plot NaI
fig = plt.figure(4, figsize=(4,6))
plt.clf()
norm = colors.Normalize(vmin=-0.5, vmax=1)
cmap = cm.ScalarMappable(norm=norm, cmap=cm.get_cmap('Blues'))
for i, pw1 in enumerate(pw):
    for j, pw2 in enumerate(pw1):
        for k, pw3 in enumerate(pw2):

            # Get rgb value for this tXtw
            rgb = cmap.to_rgba(float(i)/(len(mdot)-1))

            plt.subplot(len(expansion), len(pot), 1+k+len(pot)*j)
            plt.plot(lam[1], np.exp(-tau[i,j,k,1,:]), color=rgb, lw=2)
            if i == len(pw)-1:
                #plt.plot(lam[1], np.exp(-tau[len(mdot)/2, -1, -1, 1, :]),
                #         'r--')
                plt.plot(lam0[1][0]*np.ones(2), [0, 1], 'k:')
                plt.plot(lam0[1][1]*np.ones(2), [0, 1], 'k:')
            plt.xlim([587, 592])
            if j == 0:
                plt.ylim([0.9, 1])
            elif j == 1:
                plt.ylim([0.7, 1])
                plt.gca().set_yticks([0.7, 0.75, 0.80, 0.85, 0.9, 0.95])
            else:
                plt.ylim([0,1])
                plt.gca().set_yticks([0, 0.2, 0.4, 0.6, 0.8])
            if j != len(expansion)-1:
                plt.gca().set_xticklabels([])
            else:
                if k == 0:
                    plt.gca().set_xticks([588, 590])
                else:
                    plt.gca().set_xticks([588, 590])
                plt.xlabel(r'$\lambda$ [nm]')
            if k == 0:
                if j == 1:
                    plt.ylabel(r'Transmission fraction')
            else:
                plt.gca().set_yticklabels([])
            if j == 0:
                ax = plt.gca().twiny()
                ax.set_xlim([c*(lam[1][0]/lam0[1][0]-1)/1e8,
                             c*(lam[1][-1]/lam0[1][0]-1)/1e8])
                ax.set_xlabel(r'$v$ [10$^3$ km s$^{-1}$]')
                ax.set_xticks([-1, 0, 1])
                ax.set_title(pot[k].title(), y=1.3)
            if k == len(pot)-1:
                ax = plt.gca().twinx()
                ax.set_ylabel(expansion[j].title())
                ax.set_yticks([])
plt.subplots_adjust(hspace=0, wspace=0, left=0.2, top=0.85)
plt.savefig('m82_NaI_var.pdf')
