"""
This script generates a plot showing an example calculation of
subcritical emission.
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as colors
from despotic.winds import pwind

# Grid in u
u = np.linspace(1e-2, 3, 100)

# Grid in Gamma
mach = 50.
Gamma = np.logspace(-2,0,8)

# Plot
plt.figure(1, figsize=(5,5))
plt.clf()
norm = colors.Normalize(vmin=-0.5, vmax=1)
cmap = cm.ScalarMappable(norm=norm, cmap=cm.get_cmap('Blues'))
p=[]
lab=[]
for i, g in enumerate(Gamma):
    
    # Get rgb value for this tXtw
    rgb = cmap.to_rgba(float(len(Gamma)-i-1)/(len(Gamma)-1))

    # Create winds
    wa = pwind(g, mach, expansion='area', potential='isothermal')
    ws = pwind(g, mach, expansion='solid angle', potential='isothermal')

    # Plot
    plt.subplot(2,2,1)
    xi_a = wa.Xi(u)
    plt.plot(-u, np.log10(xi_a), lw=2, color=rgb)
    plt.plot(u, np.log10(xi_a), lw=2, color=rgb)

    plt.subplot(2,2,2)
    xi_s = ws.Xi(u)
    plt.plot(-u, np.log10(xi_s), lw=2, color=rgb)
    plt.plot(u, np.log10(xi_s), lw=2, color=rgb)

    plt.subplot(2,2,3)
    xi_a = wa.Xi(u, varpi=2)
    plt.plot(-u, np.log10(xi_a), lw=2, color=rgb)
    plt.plot(u, np.log10(xi_a), lw=2, color=rgb)

    plt.subplot(2,2,4)
    xi_s = ws.Xi(u, varpi=2)
    plt.plot(-u, np.log10(xi_s), lw=2, color=rgb)
    plt.plot(u, np.log10(xi_s), lw=2, color=rgb)

# Tweak plots
xlim = [-3,3]
ylim = np.log10(np.array([1e-3,1e4]))

plt.subplot(2,2,1)
plt.xlim(xlim)
plt.ylim(ylim)
plt.title('Area')
plt.ylabel(r'$\log\,\Xi$')
plt.gca().set_xticklabels([])

plt.subplot(2,2,2)
plt.xlim(xlim)
plt.ylim(ylim)
plt.title('Solid Angle')
plt.gca().set_xticklabels([])
plt.gca().set_yticklabels([])
plt.text(xlim[0], 2.75, r'$\varpi=0$', ha='center', size=14,
         bbox={'facecolor' : 'w'})

plt.subplot(2,2,3)
plt.xlim(xlim)
plt.ylim(ylim)
plt.xlabel(r'$u$')
plt.ylabel(r'$\log\,\Xi$')
plt.setp(plt.gca().get_xticklabels()[-1], visible=False)
plt.setp(plt.gca().get_yticklabels()[-1], visible=False)

plt.subplot(2,2,4)
plt.xlim(xlim)
plt.ylim(ylim)
plt.xlabel(r'$u$')
plt.gca().set_yticklabels([])
plt.text(xlim[0], 2.75, r'$\varpi=2$', ha='center', size=14,
         bbox={'facecolor' : 'w'})

plt.subplots_adjust(wspace=0, hspace=0, left=0.18, right=0.95)
plt.savefig('subcritical.pdf')
