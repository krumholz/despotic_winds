"""
Code to produce the plots for the example calculation for M82-like
starburst.
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colorbar as cbar
import matplotlib.cm as cm
import matplotlib.colors as colors
from scipy.optimize import brentq
from despotic import emitter
from despotic.winds import pwind, zetaM, sxMach

# Constants; switch to cgs
from scipy.constants import G, c, m_p, m_e, h
from scipy.constants import k as kB
from astropy.units import Msun, yr, Angstrom, pc
c = 1e2*c
hP = h*1e7
G = 1e3*G
kB = 1e7*kB
mH = (m_e + m_p)*1e3
muH = 1.4
Msun = Msun.to('g')
yr = yr.to('s')
Myr = 1e6*yr
Gyr = 1e9*yr
ang = Angstrom.to('cm')
pc = pc.to('cm')
kpc = 1e3*pc

# Physical parameters
mdotstar = 4.1*Msun/yr
epsff = 0.01
mach = 100.0
uh = 10.0
v0 = 120e5*np.sqrt(2)
r0 = 250*pc
m0 = v0**2*r0/(2.0*G)
rho0 = 3.0*m0/(4.0*np.pi*r0**3)
tc = r0/v0
temp = 50.
mdot = 100.0*Msun/yr
dist = 3.5e3*kpc

# Opening angle and inclination
phi = -5.0/90.0*np.pi/2.0
theta_in = 30.0/90.0*np.pi/2.0
theta_out = 50./90.*np.pi/2.0

# Get the Eddington factor and tw
eta = (np.cos(theta_in) - np.cos(theta_out)) * mdot / mdotstar
Gamma = brentq(
    lambda g: zetaM(np.log(g), sxMach(mach))/epsff - eta,
    1e-6, 100.)
tw = m0/mdot

# Create wind object
pw = pwind(Gamma, mach, driver='hot', potential='isothermal',
           expansion='solid angle', geometry='cone sheath',
           theta=theta_out, theta_in=theta_in, phi=phi, uh=uh)

# Define the spatial and velocity grids we will use
u = np.linspace(-3, 3, 100)
varpi_a = np.linspace(0, 2*kpc, 200)/r0
varpi_t = np.linspace(0, 2*kpc, 200)/r0
vpa2, vpt2 = np.meshgrid(varpi_a, varpi_t, indexing='xy')
varpi_a_prof = np.array([-2,-1.5,-1,-0.5,0.5,1,1.5,2])*kpc/r0

#######################################################################
# Halpha emission
#######################################################################

# Cooling constant
lam_e = 3.9e-25

# Wavelength grid
lam0 = 6562.801*ang
lam = lam0*(1.0+u*v0/c)

# Get line profiles along outflow axis
Ha = np.empty((varpi_a_prof.size, u.size))
for i, varpi_a_ in enumerate(varpi_a_prof):
    Ha[i,:] = 1e23*2.0/(36.*np.pi) * lam_e * r0 * \
              (rho0/mH)**2 * lam0 / v0 * \
              (tc/tw)**2 * pw.Xi(u, varpi=varpi_a_)

# Get integrated emission everywhere; mask undefined values
xi = 1.0/(36.*np.pi) * lam_e * r0 * (rho0/mH)**2 * pw.xi(vpa2, vpt2)
xi[xi > 1.0e300] = np.nan
xi[xi == 0.0] = np.nan

# Plot integrated emission
fig = plt.figure(1, figsize=(7,5))
plt.clf()
ax = plt.axes([0.1, 0.1, 0.5, 0.65])
vmin = -4
vmax = 2
xi_big = np.empty(2*np.array(xi.shape)-1)
xi_big[xi.shape[0]-1:, xi.shape[1]-1:] = xi
xi_big[:xi.shape[0], xi.shape[1]-1:] = xi[::-1,:]
xi_big[xi.shape[0]-1:, :xi.shape[1]] = xi[:,::-1]
xi_big[:xi.shape[0], :xi.shape[1]] = xi[::-1,::-1]
ax.imshow(np.transpose(np.log10(xi_big)), origin='lower', aspect='equal',
          extent=np.array([-varpi_a[-1],varpi_a[-1],
                           -varpi_a[-1],varpi_a[-1]])*r0/kpc,
          vmin=vmin, vmax=vmax,
          interpolation='nearest')
ax.set_xlim(np.array([-varpi_a[-1], varpi_a[-1]])*r0/kpc)
ax.set_ylim(np.array([-varpi_a[-1], varpi_a[-1]])*r0/kpc)
plt.pause(0.01)  # Needed due to a bug in matplotlib
                 # see http://matplotlib.1069221.n5.nabble.com/
                 # How-to-determine-position-of-axes-after-imshow-td4427.html

# Overplot points where spectra are taken
ax.plot(np.zeros(varpi_a_prof.shape)*r0/kpc, varpi_a_prof*r0/kpc, 's',
        ms=7, mec='k', mfc='w', mew=2)

# Plot masked regions
x = np.linspace(0,varpi_a[-1],1000)
z1 = np.sqrt(1.0-np.minimum(x,1.0)**2)
z2 = x*np.sqrt((cos(2*theta_out) + cos(2*phi))/
               (1.0-cos(2*theta_out)))
ax.plot(x*r0/kpc, np.maximum(z1, z2)*r0/kpc, 'k', lw=3)
ax.plot(-x*r0/kpc, np.maximum(z1, z2)*r0/kpc, 'k', lw=3)
ax.plot(x*r0/kpc, -np.maximum(z1, z2)*r0/kpc, 'k', lw=3)
ax.plot(-x*r0/kpc, -np.maximum(z1, z2)*r0/kpc, 'k', lw=3)
x1 = np.linspace(0,1, 1000)
ax.plot(x1*r0/kpc, np.sqrt(1-x1**2)*r0/kpc, 'k:')
ax.plot(x1*r0/kpc, -np.sqrt(1-x1**2)*r0/kpc, 'k:')
ax.plot(-x1*r0/kpc, np.sqrt(1-x1**2)*r0/kpc, 'k:')
ax.plot(-x1*r0/kpc, -np.sqrt(1-x1**2)*r0/kpc, 'k:')
ax.fill_between(x*r0/kpc, np.maximum(z1, z2)*r0/kpc,
                -np.maximum(z1, z2)*r0/kpc,
                color=(0.75,0.75,0.75))
ax.fill_between(-x*r0/kpc, np.maximum(z1, z2)*r0/kpc,
                -np.maximum(z1, z2)*r0/kpc,
                color=(0.75,0.75,0.75))

# Add labels
ax.set_xlabel(r'$x$ [kpc]')
ax.set_ylabel(r'$z$ [kpc]')

# Create dummy axes we can use to add labels on right
axnew = fig.add_axes(ax.get_position(), frameon=False)
axnew.yaxis.tick_right()
axnew.yaxis.set_label_position('right')
axnew.set_ylim(np.array([-varpi_a[-1], varpi_a[-1]])*r0/dist*
               360.*60.**2/(2.*np.pi))
axnew.xaxis.tick_top()
axnew.xaxis.set_label_position('top')
axnew.set_xlim(np.array([-varpi_t[-1], varpi_t[-1]])*r0/dist*
               360.*60.**2/(2.*np.pi))
axnew.set_xlabel(r'$x$ [arcsec]')
axnew.set_ylabel(r'$z$ [arcsec]')

# Add colorbar
pos = ax.get_position()
axcbar = plt.axes([pos.x0, pos.y1+0.1, pos.x1-pos.x0, 0.025])
cb = cbar.ColorbarBase(axcbar, orientation='horizontal',
                       ticklocation='top',
                       label=r'log H$\alpha$ surface brightness'
                       ' [erg s$^{-1}$ cm$^{-2}$ sr$^{-1}$]',
                       norm=colors.Normalize(vmin=vmin, vmax=vmax))
cb.set_ticks([-4, -2, 0, 2])

# Plot spectra
ax2 = plt.axes([pos.x1+0.15, pos.y0, 0.2, pos.y1-pos.y0])
nspec = Ha.shape[0]
for i in range(nspec):
    data = Ha[nspec-i-1,:] / np.amax(Ha[nspec-i-1,:])
    #ax2.plot(lam*1e7, i/float(nspec)+0.9*data/nspec, 'k')
    ax2.plot(u*v0/1e7, i/float(nspec)+0.9*data/nspec, 'k')
ax2.set_xlim([-5,5])
ax2.set_yticks([])
ax2.yaxis.set_label_position('right')
ax2.set_ylabel('Normalised flux')
ax2.set_xlabel(r'$v$ [$10^2$ km s$^{-1}$]')
ax2a = ax2.twiny()
plt.pause(0.01)  # Pauses needed here to sidestep a matplotlib bug
ax2a.set_xlim(lam0*1e7*(1.0+np.array([-5,5])*v0/c))
plt.pause(0.01)
ax2a.set_xticks([655, 656, 657, 658])
plt.pause(0.01)
ax2a.set_xticklabels(ax2a.xaxis.get_majorticklabels(), rotation=90)
plt.pause(0.01)
ax2a.set_xlabel('$\lambda$ [nm]')

# Save
plt.savefig('m82_Ha.pdf')

#######################################################################
# MgII, NaI absorption
#######################################################################

# Absorption lines
lineid = [r'Mg II $\lambda\lambda 2796, 2804$',
          r'Na I $\lambda\lambda 5892, 5898$']
lam0 = [[279.6, 280.4], [589.2, 589.8]]
u_trans = [[0, (lam0[0][1]-lam0[0][0])/lam0[0][0]*c/v0],
           [0, (lam0[1][1]-lam0[1][0])/lam0[1][0]*c/v0]]
tX = np.array([[190., 95.], [0.32, 0.16]])*Gyr

# Wavelength grids
nlam = 400
lam = [np.linspace(278., 282., nlam), np.linspace(587., 592., nlam)]

# Output holder
tau = np.zeros((len(varpi_a_prof), len(lineid), nlam))
tau1 = np.zeros((len(varpi_a_prof), len(lineid), nlam))
tau2 = np.zeros((len(varpi_a_prof), len(lineid), nlam))

# Loop over positions
for i, varpi_a_ in enumerate(varpi_a_prof):

    # Loop over lines
    for j, l0 in enumerate(lam0):

        # Compute optical depth for first doublet transition alone
        l = lam[j]
        u = (l/l0[0]-1)*c/v0
        tau1[i,j,:] = pw.tau(u, tXtw=tX[j][0]/tw, correlated=True,
                             varpi=varpi_a_)

        # Second doublet transition alone
        #u = (l/l0[1]-1)*c/v0
        tau2[i,j,:] = pw.tau(u-u_trans[j][1], tXtw=tX[j][1]/tw, correlated=True,
                             varpi=varpi_a_)

        # Compute total optical depth from combination of both components
        u = (l/l0[0]-1)*c/v0
        tau[i,j,:] = pw.tau(u, tXtw=tX[j]/tw, u_trans=u_trans[j],
                            correlated=True, varpi=varpi_a_)

fig = plt.figure(2, figsize=(4,8))
plt.clf()
for i in range(len(varpi_a_prof)):

    # Plot MgII
    ax1=plt.subplot(8,2,15-2*i)
    ax1.plot(lam[0], np.exp(-tau[i,0,:]), 'k-', lw=3)
    ax1.plot(lam[0], np.exp(-tau1[i,0,:]), 'r--', lw=1)
    ax1.plot(lam[0], np.exp(-tau2[i,0,:]), 'b--', lw=1)
    ax1.plot([lam0[0][0], lam0[0][0]], [0,1], 'r:')
    ax1.plot([lam0[0][1], lam0[0][1]], [0,1], 'b:')
    ax1.set_ylim([0,1])
    ax1.set_yticks([0, 0.25, 0.5, 0.75, 1])
    if i < len(varpi_a_prof)-1:
        ax1.set_yticklabels(['0.0', '', '0.5', '', ''])
    else:
        ax1.set_yticklabels(['0.0', '', '0.5', '', '1.0'])

    # Plot NaI
    ax2=plt.subplot(8,2,16-2*i)
    ax2.plot(lam[1], np.exp(-tau[i,1,:]), 'k-', lw=3)
    ax2.plot(lam[1], np.exp(-tau1[i,1,:]), 'r--', lw=1)
    ax2.plot(lam[1], np.exp(-tau2[i,1,:]), 'b--', lw=1)
    ax2.plot([lam0[1][0], lam0[1][0]], [0,1], 'r:')
    ax2.plot([lam0[1][1], lam0[1][1]], [0,1], 'b:')

    # Add velocity
    if i == len(varpi_a_prof)-1:
        ax1a = ax1.twiny()
        u = (lam[0]/lam0[0][0]-1)*c/v0
        ax1a.set_xlim([u[0]*v0/1e8, u[-1]*v0/1e8])
        ax1a.set_xlabel('v [$10^3$ km s$^{-1}$]')

        ax2a = ax2.twiny()
        u = (lam[1]/lam0[1][0]-1)*c/v0
        ax2a.set_xlim([u[0]*v0/1e8, u[-1]*v0/1e8])
        ax2a.set_xlabel('v [$10^3$ km s$^{-1}$]')
        ax2a.set_xticks([-1, 0, 1])

    # Tweak labels
    if i > 0:
        ax1.xaxis.set_ticklabels([])
        ax2.xaxis.set_ticklabels([])
    else:
        ax1.xaxis.set_ticks([278, 280, 282])
        ax2.xaxis.set_ticks([588, 590, 592])
        ax1.set_xlabel(r'$\lambda$ [nm]')
        ax2.set_xlabel(r'$\lambda$ [nm]')
    if i == 0:
        ax2.set_ylim([0.8, 1])
        ax2.set_yticks([0.8, 0.9])
    elif i == len(varpi_a_prof)-1:
        ax2.set_ylim([0.8, 1])
        ax2.set_yticks([0.8, 0.9, 1.0])
        ax1.set_title('Mg II', y=1.65)
        ax2.set_title('Na I', y=1.65)
    elif i == 1 or i == len(varpi_a_prof)-2:
        ax2.set_ylim([0.7, 1])
        ax2.set_yticks([0.7, 0.8, 0.9])
    elif i == 2 or i == len(varpi_a_prof)-3:
        ax2.set_ylim([0.6, 1])
        ax2.set_yticks([0.6, 0.8])
    elif i == 3 or i == len(varpi_a_prof)-4:
        ax2.set_ylim([0.4, 1])
        ax2.set_yticks([0.4, 0.7])
    ax2a = ax2.twinx()
    ax2a.set_yticks([])
    ax2a.set_ylabel('{:4.1f} kpc'.format(varpi_a_prof[i]*r0/kpc))

# Overall left axis label
ax = fig.add_subplot(1,1,1)
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.tick_params(top='off', bottom='off', left='off', right='off')
ax.patch.set_visible(False)
ax.get_xaxis().set_ticks([])
ax.get_yaxis().set_ticks([])
ax.set_ylabel(r'Transmission fraction', labelpad=30)

# Adjust and save
plt.subplots_adjust(hspace=0.1, wspace=0.35, left=0.2, top=0.9, bottom=0.07,
                    right=0.9)
plt.savefig('m82_odepth.pdf')

#######################################################################
# CO emission
#######################################################################

# Import the required molecular data
co = emitter('CO', 1.1e-4)

# Compute integrated antenna temperature
intTA = np.zeros(vpa2.shape)
try:
    data = np.load('m82_CO_int.npz')
    intTA = data['intTA']
except IOError:
    pass
for i in range(len(vpa2)):
    print i
    if np.amax(intTA[i,:]) == 0.0:
        intTA[i,:] = pw.intTA_LTE(v0/1e5, temp, emit=co, tw=tw,
                                  varpi=vpa2[i,:],
                                  varpi_t=vpt2[i,:])
    np.savez('m82_CO_int.npz', intTA=intTA)

# Fill in by symmetry
intTA_big = np.empty(2*np.array(intTA.shape)-1)
intTA_big[intTA.shape[0]-1:, intTA.shape[1]-1:] = intTA
intTA_big[:intTA.shape[0], intTA.shape[1]-1:] = intTA[::-1,:]
intTA_big[intTA.shape[0]-1:, :intTA.shape[1]] = intTA[:,::-1]
intTA_big[:intTA.shape[0], :intTA.shape[1]] = intTA[::-1,::-1]

# Get line profiles along outflow axis
CO_TB = np.empty((varpi_a_prof.size, u.size))
for i, varpi_a_ in enumerate(varpi_a_prof):
    CO_TB[i,:] = pw.temp_LTE(u, temp, emit=co, tw=tw, varpi=varpi_a_)

# Plot integrated emission
fig = plt.figure(3, figsize=(7,5))
plt.clf()
ax = plt.axes([0.1, 0.1, 0.5, 0.65])
vmin = 0
vmax = 3
ax.imshow(np.transpose(np.log10(intTA_big)), origin='lower', aspect='equal',
          extent=np.array([-varpi_a[-1],varpi_a[-1],
                           -varpi_a[-1],varpi_a[-1]])*r0/kpc,
          vmin=vmin, vmax=vmax,
          interpolation='nearest')
ax.set_xlim(np.array([-varpi_a[-1], varpi_a[-1]])*r0/kpc)
ax.set_ylim(np.array([-varpi_a[-1], varpi_a[-1]])*r0/kpc)
plt.pause(0.01)  # Needed due to a bug in matplotlib
                 # see http://matplotlib.1069221.n5.nabble.com/
                 # How-to-determine-position-of-axes-after-imshow-td4427.html

# Overplot points where spectra are taken
ax.plot(np.zeros(varpi_a_prof.shape)*r0/kpc, varpi_a_prof*r0/kpc, 's',
        ms=7, mec='k', mfc='w', mew=2)

# Plot masked regions
x = np.linspace(0,varpi_a[-1],1000)
z1 = np.sqrt(1.0-np.minimum(x,1.0)**2)
z2 = x*np.sqrt((np.cos(2*theta_out) + np.cos(2*phi))/
               (1.0-np.cos(2*theta_out)))
ax.plot(x*r0/kpc, np.maximum(z1, z2)*r0/kpc, 'k', lw=3)
ax.plot(-x*r0/kpc, np.maximum(z1, z2)*r0/kpc, 'k', lw=3)
ax.plot(x*r0/kpc, -np.maximum(z1, z2)*r0/kpc, 'k', lw=3)
ax.plot(-x*r0/kpc, -np.maximum(z1, z2)*r0/kpc, 'k', lw=3)
x1 = np.linspace(0,1, 1000)
ax.plot(x1*r0/kpc, np.sqrt(1-x1**2)*r0/kpc, 'k:')
ax.plot(x1*r0/kpc, -np.sqrt(1-x1**2)*r0/kpc, 'k:')
ax.plot(-x1*r0/kpc, np.sqrt(1-x1**2)*r0/kpc, 'k:')
ax.plot(-x1*r0/kpc, -np.sqrt(1-x1**2)*r0/kpc, 'k:')
ax.fill_between(x*r0/kpc, np.maximum(z1, z2)*r0/kpc,
                -np.maximum(z1, z2)*r0/kpc,
                color=(0.75,0.75,0.75))
ax.fill_between(-x*r0/kpc, np.maximum(z1, z2)*r0/kpc,
                -np.maximum(z1, z2)*r0/kpc,
                color=(0.75,0.75,0.75))

# Add labels
ax.set_xlabel(r'$x$ [kpc]')
ax.set_ylabel(r'$z$ [kpc]')

# Create dummy axes we can use to add labels on right
axnew = fig.add_axes(ax.get_position(), frameon=False)
axnew.yaxis.tick_right()
axnew.yaxis.set_label_position('right')
axnew.set_ylim(np.array([-varpi_a[-1], varpi_a[-1]])*r0/dist*
               360.*60.**2/(2.*np.pi))
axnew.xaxis.tick_top()
axnew.xaxis.set_label_position('top')
axnew.set_xlim(np.array([-varpi_t[-1], varpi_t[-1]])*r0/dist*
               360.*60.**2/(2.*np.pi))
axnew.set_xlabel(r'$x$ [arcsec]')
axnew.set_ylabel(r'$z$ [arcsec]')

# Add colorbar
pos = ax.get_position()
axcbar = plt.axes([pos.x0, pos.y1+0.1, pos.x1-pos.x0, 0.025])
cb = cbar.ColorbarBase(axcbar, orientation='horizontal',
                       ticklocation='top',
                       label=r'log CO intensity'
                       ' [K km s$^{-1}$]',
                       norm=colors.Normalize(vmin=vmin, vmax=vmax))
cb.set_ticks([0, 1, 2, 3])

# Plot spectra
ax2 = plt.axes([pos.x1+0.15, pos.y0, 0.2, pos.y1-pos.y0])
nspec = CO_TB.shape[0]
for i in range(nspec):
    data = CO_TB[nspec-i-1,:] / np.amax(CO_TB[nspec-i-1,:])
    ax2.plot(u*v0/1e7, i/float(nspec)+0.9*data/nspec, 'k')
#ax2.xaxis.set_ticks([-6, -3, 0, 3, 6])
ax2.set_xlim([-5, 5])
ax2.set_yticks([])
ax2.yaxis.set_label_position('right')
ax2.set_ylabel('Normalised flux')
ax2.set_xlabel('v [10$^2$ km s$^{-1}$]')

# Save
plt.savefig('m82_CO.pdf')
