"""
This script generates a plot showing example optical depths tau.
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as colors
from despotic.winds import pwind

# Values of Gamma and Mach number
Gamma = 0.2
M = 50.0

# Set up winds
wa = pwind(Gamma, M, expansion='area', potential='isothermal')
ws = pwind(Gamma, M, expansion='solid angle', potential='isothermal')

# Grid of tXtw values
tXtw = np.logspace(-1,2,7)

# Grid in u
u = np.linspace(0.001, 5, 100)

# Plot
plt.figure(1, figsize=(6,6))
plt.clf()
norm = colors.Normalize(vmin=-0.5, vmax=1)
cmap = cm.ScalarMappable(norm=norm, cmap=cm.get_cmap('Blues'))
for i in range(len(tXtw)):

    # Get rgb value for this tXtw
    rgb = cmap.to_rgba(float(i)/(len(tXtw)-1))

    # Compute optical depths
    tau_c_a = wa.tau(u, tXtw[i], correlated=True)
    tau_uc_a = wa.tau(u, tXtw[i], correlated=False)
    tau_c_s = ws.tau(u, tXtw[i], correlated=True)
    tau_uc_s = ws.tau(u, tXtw[i], correlated=False)

    # Plot optical depths
    plt.subplot(2,2,1)
    p1,=plt.plot(-u, np.log10(tau_c_a), linestyle='-',
                 color=rgb, lw=2)
    p2,=plt.plot(-u, np.log10(tau_uc_a), linestyle='--',
                 color=rgb, lw=2)
    if i == len(tXtw)-1:
        plt.legend([p1,p2], ['Correlated', 'Uncorrelated'],
                   loc='upper left', prop={'size':10})

    plt.subplot(2,2,2)
    plt.plot(-u, np.log10(tau_c_s), ls='-', color=rgb, lw=2)
    plt.plot(-u, np.log10(tau_uc_s), ls='--', color=rgb, lw=2)

    # Plot transmission
    plt.subplot(2,2,3)
    plt.plot(-u, np.exp(-tau_c_a), ls='-', color=rgb, lw=2)
    plt.plot(-u, np.exp(-tau_uc_a), ls='--', color=rgb, lw=2)
    
    plt.subplot(2,2,4)
    plt.plot(-u, np.exp(-tau_c_s), ls='-', color=rgb, lw=2)
    plt.plot(-u, np.exp(-tau_uc_s), ls='--', color=rgb, lw=2)

# Tweak plots
plt.subplot(2,2,1)
plt.ylim([-2.5,0.5])
plt.gca().set_xticklabels([])
plt.ylabel(r'$\log\,\tau_\nu$')
plt.title('Area')
plt.setp(plt.gca().get_yticklabels()[0], visible=False)

plt.subplot(2,2,2)
plt.ylim([-2.5,0.5])
plt.gca().set_xticklabels([])
plt.gca().set_yticklabels([])
plt.title('Solid Angle')
plt.setp(plt.gca().get_yticklabels()[0], visible=False)

plt.subplot(2,2,3)
plt.ylim([0.5, 1])
plt.ylabel('Transmission')
plt.xlabel(r'$u$')

plt.subplot(2,2,4)
plt.ylim([0.5, 1])
plt.gca().set_yticklabels([])
plt.xlabel(r'$u$')
plt.setp(plt.gca().get_xticklabels()[0], visible=False)

plt.subplots_adjust(wspace=0, hspace=0, left=0.15, right=0.95)

plt.savefig('odepth.pdf')
