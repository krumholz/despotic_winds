"""
This script plots some examples of velocity-integrated line emission
from a gas in LTE. This script takes a while to run, since it is
effectively doing a lot of triple integrals.
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as colors
from despotic import emitterData, emitter
from despotic.winds import pwind
import scipy.constants as physcons
me = physcons.m_e * 1e3
mH = physcons.m_p * 1e3 + me
Msun = 1.99e33
pc = 3.09e18

# Gamma and Mach
Gamma = 0.2
Mach = 50.0

# Set up winds
wa = pwind(Gamma, Mach, expansion='area', potential='isothermal',
           epsrel=1e-2)
ws = pwind(Gamma, Mach, expansion='solid angle',
           potential='isothermal', epsrel=1e-2)

# Grab CO data
co = emitter('co', 1.1e-4)
mX = 1.4*mH / co.abundance
tX = co.tX(mX, trans=0)

# Grid of tw values
tXtw = np.logspace(-1,2,7)
tw = tX/tXtw

# Kinetic temperature
Tk = 50.0
co.setLevPopLTE(Tk)

# Grid in varpi
varpi = np.linspace(0, 4, 100)

# Compute integrated intensities
intTA_a = np.zeros((len(tw), len(varpi)))
intTA_s = np.zeros((len(tw), len(varpi)))
intTA_a_uc = np.zeros((len(tw), len(varpi)))
intTA_s_uc = np.zeros((len(tw), len(varpi)))
if True:
    for i in range(len(tw)):
        print i
        try:
            data = np.load('LTE_int_thick.npz')
            idx = data['intTA_a'] != 0.0
            intTA_a[idx] = data['intTA_a'][idx]
            intTA_s[idx] = data['intTA_s'][idx]
            intTA_a_uc[idx] = data['intTA_a_uc'][idx]
            intTA_s_uc[idx] = data['intTA_s_uc'][idx]
        except IOError:
            pass
        if np.max(intTA_a[i,:]) > 0:
            continue
        print 'a'
        intTA_a[i,:] = wa.intTA_LTE(1.0, Tk, emit=co, tw=tw[i],
                                    varpi=varpi)
        print 's'
        intTA_s[i,:] = ws.intTA_LTE(1.0, Tk, emit=co, tw=tw[i],
                                    varpi=varpi)
        print 'a uc'
        intTA_a_uc[i,:] = wa.intTA_LTE(1.0, Tk, emit=co, tw=tw[i],
                                       varpi=varpi, correlated=False)
        print 's uc'
        intTA_s_uc[i,:] = ws.intTA_LTE(1.0, Tk, emit=co, tw=tw[i],
                                       varpi=varpi, correlated=False)
        np.savez('LTE_int_thick.npz',
                 intTA_a=intTA_a, intTA_s=intTA_s,
                 intTA_a_uc=intTA_a_uc, intTA_s_uc=intTA_s_uc)

    # Compute X factors
    intTA_a_thin = np.zeros((len(tw), len(varpi)))
    intTA_s_thin = np.zeros((len(tw), len(varpi)))
    intTA_a_uc_thin = np.zeros((len(tw), len(varpi)))
    intTA_s_uc_thin = np.zeros((len(tw), len(varpi)))
    for i in range(len(tw)):
        print 'thin', i
        try:
            data = np.load('LTE_int_thin.npz')
            idx_thin = data['intTA_a_thin'] != 0.0
            intTA_a_thin[idx] = data['intTA_a_thin'][idx]
            intTA_s_thin[idx] = data['intTA_s_thin'][idx]
            intTA_a_uc_thin[idx] = data['intTA_a_uc_thin'][idx]
            intTA_s_uc_thin[idx] = data['intTA_s_uc_thin'][idx]
        except IOError:
            pass
        if np.max(intTA_a_thin[i,:]) > 0:
            continue
        print 'a'
        intTA_a_thin[i,:] = wa.intTA_LTE(1.0, Tk, emit=co, tw=tw[i],
                                         varpi=varpi, thin=True)
        print 's'
        intTA_s_thin[i,:] = ws.intTA_LTE(1.0, Tk, emit=co, tw=tw[i],
                                         varpi=varpi, thin=True)
        print 'a uc'
        intTA_a_uc_thin[i,:] = wa.intTA_LTE(1.0, Tk, emit=co, tw=tw[i],
                                            varpi=varpi, thin=True,
                                            correlated=False)
        print 's uc'
        intTA_s_uc_thin[i,:] = ws.intTA_LTE(1.0, Tk, emit=co, tw=tw[i],
                                            varpi=varpi, thin=True,
                                            correlated=False)
        np.savez('LTE_int_thin.npz',
                 intTA_a_thin=intTA_a_thin, intTA_s_thin=intTA_s_thin,
                 intTA_a_uc_thin=intTA_a_uc_thin,
                 intTA_s_uc_thin=intTA_s_uc_thin)
    X_a = co.Xthin(trans=0) / co.levPop[1] * (intTA_a_thin / intTA_a)
    X_s = co.Xthin(trans=0) / co.levPop[1] * (intTA_s_thin / intTA_s)
    X_a_uc = co.Xthin(trans=0) / co.levPop[1] * (intTA_a_uc_thin / intTA_a_uc)
    X_s_uc = co.Xthin(trans=0) / co.levPop[1] * (intTA_s_uc_thin /
                                                 intTA_s_uc)

    # Plot
    plt.figure(1, figsize=(5,5))
    plt.clf()
    norm = colors.Normalize(vmin=-0.5, vmax=1)
    cmap = cm.ScalarMappable(norm=norm, cmap=cm.get_cmap('Blues'))
    for i in range(len(tw)):

        # Get rgb value for this tXtw
        rgb = cmap.to_rgba(float(i)/(len(tw)-1))

        # Plot
        plt.subplot(2,2,1)
        p1,=plt.plot(varpi, np.log10(intTA_a[i,:]), ls='-', color=rgb, lw=2)
        p2,=plt.plot(varpi, np.log10(intTA_a_uc[i,:]), ls='--', color=rgb, lw=2)

        plt.subplot(2,2,2)
        plt.plot(varpi, np.log10(intTA_s[i,:]), ls='-', color=rgb, lw=2)
        plt.plot(varpi, np.log10(intTA_s_uc[i,:]), ls='--', color=rgb, lw=2)

        plt.subplot(2,2,3)
        plt.plot(varpi, np.log10(X_a[i,:]), ls='-', color=rgb, lw=2)
        plt.plot(varpi, np.log10(X_a_uc[i,:]), ls='--', color=rgb, lw=2)

        plt.subplot(2,2,4)
        plt.plot(varpi, np.log10(X_s[i,:]), ls='-', color=rgb, lw=2)
        plt.plot(varpi, np.log10(X_s_uc[i,:]), ls='--', color=rgb, lw=2)
        if i == len(tXtw)-1:
            plt.legend([p1,p2], ['Correlated', 'Uncorrelated'],
                       loc='upper right', prop={'size':10})

    # Tweak plots
    plt.subplot(2,2,1)
    plt.title('Area')
    plt.ylabel(r'$\log\, \int\, T_A\,dv/v_0 T$')
    plt.setp(plt.gca().get_yticklabels()[0], visible=False)
    plt.gca().set_xticklabels([])
    plt.ylim([-2,2])

    plt.subplot(2,2,2)
    plt.title('Solid Angle')
    plt.ylim([-2,2])
    plt.gca().set_xticklabels([])
    plt.gca().set_yticklabels([])

    plt.subplot(2,2,3)
    plt.ylim([19.2, 20.6])
    plt.xticks([0,1,2,3,4])
    plt.ylabel(r'$\log\,X_{\mathrm{CO}}$ [cm$^{-2}$ / K km s$^{-1}$]')
    plt.xlabel(r'$\varpi$')

    plt.subplot(2,2,4)
    plt.ylim([19.2, 20.6])
    plt.xticks([1,2,3,4])
    plt.gca().set_yticklabels([])
    plt.xlabel(r'$\varpi$')
    plt.twinx()
    plt.ylim(np.log10(10.**np.array([19.2, 20.6])*1.4*mH/(Msun/pc**2)))
    plt.ylabel(r'$\log\,\alpha_{\mathrm{CO}}$ [$M_\odot$ pc$^{-2}$ / K km s$^{-1}$]')

    plt.subplots_adjust(wspace=0, hspace=0, left=0.2, right=0.85)

    plt.savefig('CO_line_int.pdf')
