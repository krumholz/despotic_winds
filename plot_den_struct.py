"""
This make a plot illustrating the density structure of the wind
"""

import numpy as np
from scipy.optimize import brentq
import matplotlib.pyplot as plt
from despotic.winds import pwind, zetaM, pA
import matplotlib.cm as cm
import matplotlib.colors as colors

# Values of Gamma and Mach number
Gamma = 0.2
M = 50.0
w = pwind(Gamma, M, potential='isothermal', expansion='solid angle')

# Grid in a and u
a = np.linspace(1, 10, 200)
u = np.linspace(1e-4, 4.5, 300)
aa, uu = np.meshgrid(a, u, indexing='ij')

# Get x to go with each a, u
x = w.X(uu, aa)

# Get drho/dx
drhodx = w.drhodx(x, aa)

# Figure out values of x that correspond to fixed fractions of the mass flux
xline = [brentq(lambda x: zetaM(x, w.sx)/w.zetaM - f, -100, w.xcrit)
         for f in np.arange(0.1, 1.0, 0.1)] + \
             [w.xcrit]

# Set up plot
plt.figure(1, figsize=(6,5))
plt.clf()

# Make color map of rho
plt.imshow(np.transpose(np.log10(drhodx)), vmin=-5, vmax=-1,
           extent=[a[0], a[-1], u[0], u[-1]], origin='lower',
           aspect='auto')
plt.colorbar(label=r'$\rho_{\mathrm{norm}}^{-1} (d\rho_{\mathrm{mean}}/dx)$',
             ticks=np.linspace(-5,-1,9))

# Overplot velocities - mass lines
for x1 in xline:
    plt.plot(a, w.U(x1, a), 'k')

# Overplot lines of constant LOS velocity
uLOS = 1.5
plt.plot(a, uLOS+a*0, 'k-.', lw=2)
varpi = aa*np.sqrt(1.0-(uLOS/uu)**2)
cs=plt.contour(aa, uu, varpi, levels=[1, 2, 4],
               colors='k', linestyles='dashdot', linewidths=2)
plt.clabel(cs, inline=1, fmt=r'$\varpi = %3.1f$')

# Set limits
plt.ylim([u[0], u[-1]])

# Add axis labels
plt.xlabel(r'$a$')
plt.ylabel(r'$u_r$')

# Save
plt.savefig('den_struct.pdf')


# Plot along a cuts at constant a

# Values to plot
a = np.exp((np.arange(12)+1)*0.25)

plt.figure(2, figsize=(6,5))
plt.clf()
norm = colors.Normalize(vmin=-0.5, vmax=1)
cmap = cm.ScalarMappable(norm=norm, cmap=cm.get_cmap('Blues'))

for a_ in a:

    # Color for this a
    rgb = cmap.to_rgba((a[-1]-a_)/a[-1])

    # Get x, drho/dx
    x = w.X(u, a_)
    drhodx = w.drhodx(x, a_)

    # Plot
    plt.subplot(2,1,1)
    plt.plot(u, x, lw=2, color=rgb)

    plt.subplot(2,1,2)
    plt.plot(u, np.log10(a_**2*drhodx), lw=2, color=rgb)

# Tweak plots
plt.subplot(2,1,1)
plt.plot(u, w.xcrit*np.ones(u.size), 'k--', lw=2)
plt.ylim([-6.5,-1])
plt.ylabel(r'$x$')
plt.gca().set_xticklabels([])

plt.subplot(2,1,2)
plt.ylabel(r'$\log\,(a^2 \,d\rho_{\mathrm{mean}}/dx)$')
plt.ylim([-5,0])
#plt.gca().set_yticks([-3,-2,-1,0])
plt.xlabel(r'$u_r$')

#plt.subplots_adjust(wspace=0, hspace=0, top=0.95, left=0.15)
plt.savefig('den_struct_cut.pdf')
